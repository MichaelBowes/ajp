package de.uniba.wiai.dsg.ajp.assignment1.validation.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.uniba.wiai.dsg.ajp.assignment1.validation.ValidationResult;
import de.uniba.wiai.dsg.ajp.assignment1.validation.Warning;

/**
 * @author Michael Bowes
 *
 *         This class contains methods for outputting data from
 *         ValidationResults.
 */
public class ValidationOutput {

	/**
	 * Creates a file if it does not already exist and writes the data from the
	 * given ValidationResult in it.
	 * 
	 * @param outputURL
	 *            The complete path to the output file
	 * @param result
	 *            The results to write into the CSV file
	 */
	public void writeFile(String outputURL, ValidationResult result) {

		try {
			File f = new File(outputURL + ".csv");
			f.getParentFile().mkdirs();
			f.createNewFile(); // creates file iff it does not already exist.

			String resultStr = "File , LineNumber , Warning\n";
			BufferedWriter buffWriter = new BufferedWriter(new FileWriter(outputURL + ".csv"));
			buffWriter.write(resultStr);
			for (Warning w : result.getWarnings()) {
				buffWriter.write(w.getFileName() + ", " + w.getLineNumber() + ", " + w.getType() + "\n");
			}

			buffWriter.close();

		} catch (IOException e) {
			System.out.println("Could not create file");
		}

	}

	/**
	 * Outputs the data from the ValidationResult to the console.
	 * 
	 * @param result
	 *            The results to output into the console
	 */
	public void writeLines(ValidationResult result) {

		System.out.println("File , LineNumber , Warning");
		for (Warning w : result.getWarnings()) {
			System.out.println(w.getFileName() + "," + w.getLineNumber() + "," + w.getType().toString());
		}
	}
}
