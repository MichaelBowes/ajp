package de.uniba.wiai.dsg.ajp.assignment1.validation.impl;

public interface LineChecker {
	
	public boolean check(String line);
}
