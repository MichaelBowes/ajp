package de.uniba.wiai.dsg.ajp.assignment1.validation.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Nicolas Bruch
 *
 */
public class ValidationFileReader {

	private Map<String,List<String>> files;
	private final String fileExtension = ".java";
	
	/**
	 * Creates a map containing the file URL and a list of lines
	 * from a given folder URL.
	 * @param fileURL
	 * @return
	 */	
	public Map<String,List<String>> readFile(String folderURL){	
		files = new HashMap<String,List<String>>();
		traverseFolders(folderURL);
		return this.files;
	}
	
	private void traverseFolders(String folderURL){
		Path folder = Paths.get(folderURL);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {
			for (Path path : stream) {
				if (Files.isDirectory(path)) { // new folder found
					traverseFolders(path.toString());
				} else if (Files.isRegularFile(path)) {					
					if (path.getFileName().toString().endsWith(fileExtension)) { //.java file found
						List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
						files.put(path.toString(), lines);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
