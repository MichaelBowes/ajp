package de.uniba.wiai.dsg.ajp.assignment1.validation.impl;

import java.util.List;
import java.util.Map;

import de.uniba.wiai.dsg.ajp.assignment1.validation.CodeValidationException;
import de.uniba.wiai.dsg.ajp.assignment1.validation.CodeValidator;
import de.uniba.wiai.dsg.ajp.assignment1.validation.ValidationResult;
import de.uniba.wiai.dsg.ajp.assignment1.validation.ValidationTask;
import de.uniba.wiai.dsg.ajp.assignment1.validation.Warning;
import de.uniba.wiai.dsg.ajp.assignment1.validation.WarningType;
import de.uniba.wiai.dsg.ajp.assignment1.validation.io.ValidationFileReader;
import de.uniba.wiai.dsg.ajp.assignment1.validation.io.ValidationOutput;

/**
 * @author Nicolas Bruch
 *
 */
public class SimpleCodeValidator implements CodeValidator {

	public SimpleCodeValidator() {
		/*
		 * DO NOT REMOVE
		 *
		 * REQUIRED FOR GRADING
		 */
	}

	@Override
	public ValidationResult validate(final ValidationTask task) throws CodeValidationException {
		ValidationFileReader fileReader = new ValidationFileReader();
		
		Map<String,List<String>> files = fileReader.readFile(task.getRootFolder());
		ValidationResult result = new ValidationResult();
		
		//Iterate over all files
		for(Map.Entry<String, List<String>> file : files.entrySet()){
			CharacterChecker characterChecker = new CharacterChecker();
			IndentationChecker indentationChecker = new IndentationChecker();
			SpaceChecker spaceChecker = new SpaceChecker();
			
			//Iterate over all lines
			int lineCount = 0;
			for(String line : file.getValue()){
				lineCount++;
				if(!characterChecker.check(line))
					result.addWarning(new Warning(file.getKey(), lineCount, WarningType.TOO_MANY_CHARACTERS));
				
				if(!indentationChecker.check(line))
					result.addWarning(new Warning(file.getKey(), lineCount, WarningType.FAULTY_INDENTATION));
				
				if(!spaceChecker.check(line))
					result.addWarning(new Warning(file.getKey(), lineCount, WarningType.MISSING_SPACE_AROUND_COMPARATOR));
			}
		}
		//Outputting data
		ValidationOutput output = new ValidationOutput();
		output.writeFile(task.getResultFile(), result);
		output.writeLines(result);
		
		return result;
	}

}
