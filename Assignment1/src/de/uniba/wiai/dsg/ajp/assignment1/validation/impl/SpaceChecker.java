package de.uniba.wiai.dsg.ajp.assignment1.validation.impl;

/**
 * @author Collins Kamgaing
 *
 */
public class SpaceChecker implements LineChecker {

	@Override
	public boolean check(String line) {
		if (line == null || line.length() == 0)
			throw new IllegalArgumentException();

		int index = line.indexOf('=');

		if (checkExpression(line, index, '=')) {
			index = line.indexOf('!');
			return checkExpression(line, index, '!');
		}

		return checkExpression(line, index, '!');

	}

	static boolean checkExpression(String expression, int index, char c) {

		int size = expression.length();

		if (index == -1)
			return true;

		String newString = expression.substring(0, index) + 'z' + expression.substring(index + 1);

		// solange "=" bzw. "!" in expression vorkommt
		while (index != -1) { 

			int nextIndex = index + 1;
			int next2Index = nextIndex + 1;
			int next3Index = next2Index + 1;

			int prevIndex = index - 1;
			int prev2Index = prevIndex - 1;

			if (nextIndex < size) {

				// solange "==" bzw. "!=" in expression vorkommt
				if (newString.charAt(nextIndex) == '=') {
					
					// remplace the first operator with z ex: original "==" after "z="
					newString = newString.substring(0, nextIndex) + 'z' + newString.substring(nextIndex + 1);

					// wenn vor bzw. nach dem "!=" bzw. "==" kein Leerzeichen vorkommt
					if (next2Index >= size || prevIndex < 0) 
						return false;

					 // wenn direkt vor dem "!=" bzw. "==" kein Leerzeichen vorkommt
					if (newString.charAt(prevIndex) != ' ') {
						return false;
					}

					// wenn vor dem erste Leerzeichen noch eine kommt bsp: "  == "
					if (prev2Index >= 0) {
						if (newString.charAt(prev2Index) == ' ') { 
							return false;
						}
					}

					// wenn direkt nach dem "!=" bzw. "==" kein Leerzeichen vorkommt
					if (next2Index < size && newString.charAt(next2Index) != ' ') { 
						return false;
					}
					
					// wenn nach dem erste Leerzeichen noch eine kommt bsp: " ==  "
					if (next3Index < size && newString.charAt(next3Index) == ' ') { 
						return false;
					}
				}

				index = newString.indexOf(c);
				if (index != -1) {
					newString = newString.substring(0, index) + 'z' + newString.substring(index + 1);
				}
			} else {
				return false;
			}
		}

		return true;
	}
}
