package de.uniba.wiai.dsg.ajp.assignment1.validation.impl;

/**
 * @author Michael Bowes
 * 
 *         Predicate that validates if a given line exceeds the maximum of 120
 *         characters. Note that tabs (\t) count as 4 characters. Returns false
 *         if the max. is exceeded.
 */
public class CharacterChecker implements LineChecker {
	
	private final int linelimit = 120;

	@Override
	public boolean check(String line) {
		int count = 0;
		
		for (char c : line.toCharArray()) {
			if (c == '\t') {
				count += 4;
			} else {
				count += 1;
			}
		}

		if (count > linelimit) {
			return false;
		} else {
			return true;
		}
	}

}
