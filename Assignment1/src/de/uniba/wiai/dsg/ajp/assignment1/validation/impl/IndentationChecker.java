package de.uniba.wiai.dsg.ajp.assignment1.validation.impl;


/**
 * @author Nicolas Bruch
 *
 *	This class contains a method for checking the indentation level of a string
 *	and keeps track of the indentation level for the next line.
 */
public class IndentationChecker implements LineChecker {

	private int indentationLevel = 0;
	
	@Override
	public boolean check(String line){
		//Line is empty
		if(line.equals(""))
			return true;	
		
		int startIndentationLevel = this.indentationLevel; //can't use the updated number.
		int tabs = 0;
		
		//counting the tab characters in the beginning of the line.
		boolean countingTabs = true;
		for(int i = 0; i < line.length(); i++){
			char c = line.charAt(i);
			if(countingTabs){	//Only count until a character other than \t was found.
				if(c != '\t')
					countingTabs = false;
										
				if(c == '\t')
					tabs++;
			}			
			if(c == '{')
				this.indentationLevel++;
			
			if(c == '}')
				this.indentationLevel--;
		}
		
		
		if(tabs == startIndentationLevel)
			return true;
		
		return false;
	}
}
