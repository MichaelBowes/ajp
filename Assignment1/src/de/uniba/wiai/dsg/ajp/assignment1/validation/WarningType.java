package de.uniba.wiai.dsg.ajp.assignment1.validation;

/**
 * Describes the types of warnings this validation tool checks for
 */
public enum WarningType {
	TOO_MANY_CHARACTERS, MISSING_SPACE_AROUND_COMPARATOR, FAULTY_INDENTATION
}
