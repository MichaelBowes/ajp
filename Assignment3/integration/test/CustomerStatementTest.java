package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Discount;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Movie;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;
import de.uniba.wiai.dsg.ajp.assignment3.Util.FileReader;
import junit.framework.Assert;

class CustomerStatementTest {

	private static final String URLTESTFILESTATEMENT = "integration/test/TestDataStatement.txt";
	private static final String URLTESTFILEHTMLSTATEMENT = "integration/test/TestDataHtmlStatement.txt";
	static Customer customer = new Customer("Customer name");
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {				
		Movie movie1 = new Movie("movie1", 0, MovieQuality.HD);
		Movie movie2 = new Movie("movie2", 1, MovieQuality.HD);
		Movie movie3 = new Movie("movie3", 2, MovieQuality.HD);
		Movie movie4 = new Movie("movie4", 3, MovieQuality.HD);
		Movie movie5 = new Movie("movie5", 0, MovieQuality.SD);
		Movie movie6 = new Movie("movie5", 1, MovieQuality.SD);
		Movie movie7 = new Movie("movie5", 2, MovieQuality.SD);
		Movie movie8 = new Movie("movie5", 3, MovieQuality.SD);
		movie2.setPriceCode(1);
		movie3.setPriceCode(2);
		movie4.setPriceCode(2);
		movie5.setPriceCode(2);
		movie6.setPriceCode(3);
		movie7.setPriceCode(3);
		movie8.setPriceCode(2);
		Rental rental1 = new Rental(customer, movie1);	
		Rental rental2 = new Rental(customer, movie2);
		Rental rental3 = new Rental(customer, movie3);
		Rental rental4 = new Rental(customer, movie4);
		Rental rental5 = new Rental(customer, movie5);
		Rental rental6 = new Rental(customer, movie6);
		Rental rental7 = new Rental(customer, movie7);
		Rental rental8 = new Rental(customer, movie8);
		rental1.setDiscount(Discount.HIGH);
		rental1.setDaysRented(6);
		rental2.setDiscount(Discount.LOW);
		rental2.setDaysRented(14);
		rental3.setDiscount(Discount.LOW);
		rental3.setDaysRented(22);
		rental4.setDaysRented(12);		
		rental5.setDiscount(Discount.LOW);		
		rental6.setDaysRented(5);		
		rental7.setDiscount(Discount.NONE);		
		List<Rental> rentals = new LinkedList<Rental>();
		rentals.add(rental1);
		rentals.add(rental2);
		rentals.add(rental3);
		rentals.add(rental4);
		rentals.add(rental5);
		rentals.add(rental6);
		rentals.add(rental7);
		rentals.add(rental8);
		customer.setRentals(rentals);	
	}

	@Test
	void testStatement() {
		String testData;
		try {
			testData = FileReader.read(URLTESTFILESTATEMENT);
		} catch (IOException e) {
			e.printStackTrace();
			testData = null;
		}		
		assertNotNull("Testdata could not be read", testData);
		assertEquals(customer.statement(), testData);
	}
	
	@Test
	void testHtmlStatement() {
		String testData;
		try {
			testData = FileReader.read(URLTESTFILEHTMLSTATEMENT);
		} catch (IOException e) {
			e.printStackTrace();
			testData = null;
		}		
		assertNotNull("Testdata could not be read", testData);
		assertEquals(customer.htmlStatement(), testData);
	}

}
