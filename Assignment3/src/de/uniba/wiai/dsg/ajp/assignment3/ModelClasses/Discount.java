package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

/**
 * This enum represents the different types of discounts.
 */
public enum Discount {
	/**
	 * A high discount is 50%
	 */
	HIGH,
	/**
	 * A low discount is 30%
	 */
	LOW,
	/**
	 * No discount means 0%
	 */
	NONE;
}
