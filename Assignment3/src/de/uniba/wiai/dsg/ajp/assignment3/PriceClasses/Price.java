package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;

/**
 * This abstract class represents the price of a movie.<br>
 * It serves as base for the different types of prices.
 */
public abstract class Price {

	/**
	 * The VAT that will be calculated on the final price.
	 */
	protected double valueAddedTax = 19.0;

	public void setTax(double tax) {
		valueAddedTax = tax;
	}

	/**
	 * Returns the amount of tax added to the final price.<br>
	 * The amount of the tax will depend on the final price.
	 * 
	 * @param daysRented
	 *            - Days the movie was rented.
	 * @param daysRented
	 *            - The number of days the movie was rented.
	 * @return Amount of the value added tax for the movie.
	 */
	public double getTaxAmount(int daysRented, MovieQuality quality) {
		double result = getCharge(daysRented, quality);
		result = (result * valueAddedTax) / 100;
		return result;
	}

	/**
	 * Returns the charge for the movie.<br>
	 * The amount of the charge depends on the type of the movie, the number of days
	 * the movie was rented, the tax and the given discount.
	 * 
	 * @param daysRented
	 *            - The number of days the movie was rented.
	 * @param quality
	 *            - The picture quality of the movie.
	 * @return The total charge for the movie for a given time.
	 */
	public abstract double getCharge(int daysRented, MovieQuality quality);

	/**
	 * Returns the number of frequent renter points for the movie.<br>
	 * The number of points returned is depends from the type of the movie and the
	 * amount of time for which it was rented, as well as if discounts<br>
	 * were given and if the customer watched a series before.
	 * 
	 * @param daysRented
	 *            - The number of days the movie was rented.
	 * @return The number of frequent renter points for a movie.<br>
	 *         The amount is dependent on the type of movie and the days it was
	 *         rented.<br>
	 *         The default is 1.
	 */
	public int getFrequentRenterPoints(int daysRented, Rental rental, Customer customer) {
		return 1;
	}

	/**
	 * Returns the price code of the movie.
	 * 
	 * @return The price code for the movie.<br>
	 *         The price codes are:<br>
	 *         REGULAR = 0<br>
	 *         NEW_RELEASE = 1<br>
	 *         CHILDRENS = 2<br>
	 *         SERIES = 3
	 */
	public abstract int getPriceCode();

}
