package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.*;

/**
 * This class represents a movie rented by a customer.<br>
 * A movie has a price type and a title.
 */
public class Movie {

	/**
	 * Price code for a children's type movie.<br>
	 * Value = 2
	 */
	public static final int CHILDRENS = 2;
	/**
	 * Price code for a regular type movie.<br>
	 * Value = 0
	 */
	public static final int REGULAR = 0;
	/**
	 * Price code for a new release type movie.<br>
	 * Value = 1
	 */
	public static final int NEW_RELEASE = 1;
	/**
	 * Price code for a series type.<br>
	 * Value = 3
	 */
	public static final int SERIES = 3;

	/**
	 * Represents the pricing type of the movie.
	 */
	private Price price;

	/**
	 * The title of the movie.
	 */
	private String title;

	/**
	 * The picture quality of the movie.<br>
	 */
	private MovieQuality quality;

	public Movie(String title, int priceCode, MovieQuality quality) {
		if (title == null || title.length() == 0) {
			throw new IllegalArgumentException("Movie's title cannot be empty or null");
		}
		this.title = title;
		this.setPriceCode(priceCode);
		if (quality != null) {
			this.quality = quality;
		} else {
			this.quality = MovieQuality.SD;
		}
	}

	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title of the movie only if<br>
	 * it is not null and not empty.
	 * 
	 * @param title
	 *            - the movie's title to be set.
	 */
	public void setTitle(String title) {
		if (title == null || title.length() == 0) {
			throw new IllegalArgumentException("Movie's title cannot be empty");
		}
		this.title = title;
	}

	/**
	 * Calls the {@link Price#getCharge(int)} method of the price assigned to the
	 * movie.
	 * 
	 * @param daysRented
	 *            - Number of days the movie was rented.
	 * @return The total charge for the movie for a given time.
	 * @throws IllegalArgumentException
	 *             - If the value for days rented is negative.
	 */
	public double getCharge(int daysRented) {
		if (daysRented < 0) {
			throw new IllegalArgumentException("DaysRented cannot be a negative value");			
		}
		return price.getCharge(daysRented, quality);
	}

	/**
	 * Calls the {@link Price#getTaxAmount(int, MovieQuality)} method of the
	 * price<br>
	 * and returns the amount of the tax for a given amount of days the movie was
	 * rented.
	 * 
	 * @param daysRented
	 *            - Number of days the movie was rented.
	 * @return The total tax for the movie for a given time.
	 */
	public double getTaxAmount(int daysRented) {
		if (daysRented < 0) {
			throw new IllegalArgumentException("DaysRented cannot be a negative value");			
		}
		return price.getTaxAmount(daysRented, quality);
	}

	public int getPriceCode() {
		return price.getPriceCode();
	}

	/**
	 * Sets the price code for the movie.
	 * 
	 * @param priceCode
	 *            - Price code for the movie. Allowed price codes are:<br>
	 *            The price codes are:<br>
	 *            REGULAR = 0<br>
	 *            NEW_RELEASE = 1<br>
	 *            CHILDRENS = 2<br>
	 *            SERIES = 3<br>
	 * 
	 * @throws IllegalArgumentException
	 *             If the given value is not within the stated range.
	 */
	public void setPriceCode(int priceCode) {
		if (priceCode < 0) {
			throw new IllegalArgumentException("PriceCode cannot be a negative value");
		}

		switch (priceCode) {
			case REGULAR:
				price = new RegularPrice();
				break;
			case CHILDRENS:
				price = new ChildrensPrice();
				break;
			case NEW_RELEASE:
				price = new NewReleasePrice();
				break;
			case SERIES:
				price = new SeriesPrice();
				break;
			default:
				throw new IllegalArgumentException("Incorrect Price Code");
		}
	}

	/**
	 * Sets movie quality if it's not null.
	 * 
	 * @param quality
	 *            - the quality of the movie.
	 */
	public void setQuality(MovieQuality quality) {
		if (quality == null) {
			throw new IllegalArgumentException("Quality mustn't be set to null");
		}
		this.quality = quality;
	}

	public MovieQuality getQuality() {
		return quality;
	}

	/**
	 * Calls the {@link Price #getFrequentRenterPoints(int)} method of the price
	 * assigned to the movie.
	 * 
	 * @param daysRented
	 *            - The number of days the movie was rented.
	 * @param rental
	 *            - The rental of the customer.
	 * @param customer
	 *            - The customer who rents the movie.
	 * @return The price code for the movie.<br>
	 *         The price codes are:<br>
	 *         REGULAR = 0<br>
	 *         NEW_RELEASE = 1<br>
	 *         CHILDRENS = 2<br>
	 *         SERIES = 3<br>
	 */
	public int getFrequentRenterPoints(int daysRented, Rental rental, Customer customer) {
		return price.getFrequentRenterPoints(daysRented, rental, customer);
	}

	public Price getPrice() {
		return price;
	}

}
