package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Movie;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;

/**
 * This class represents the pricing type for a series.<br>
 * The price code for this class is 3.
 */
public class SeriesPrice extends Price {

	@Override
	public double getCharge(int daysRented, MovieQuality quality) {
		if (quality == null) {
			throw new IllegalArgumentException("The Quality of the Movie mustn't be null");
		}
		double result = 3.5;
		if (daysRented == 2) {
			result += 3;
		} else if (daysRented > 2) {
			result += (daysRented - 2) * 1.5 + 3;
		}
		if (quality == MovieQuality.HD) {
			result += 2;
		}
		return result;
	}

	@Override
	public int getFrequentRenterPoints(int daysRented, Rental rental, Customer customer) {
		int renterpoints = 1;
		if (customer.getRentedSeriesOnce() == false) {
			renterpoints += 3;
			customer.setRentedSeriesOnce(true);
		}
		if (rental.getDiscount() == 0.7 || rental.getDiscount() == 0.5) {
			renterpoints += 1;
		} else {
			return renterpoints;
		}
		return renterpoints;
	}

	@Override
	public int getPriceCode() {
		return Movie.SERIES;
	}

}
