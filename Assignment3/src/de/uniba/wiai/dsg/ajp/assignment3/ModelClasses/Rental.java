package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.Price;

/**
 * This class represents the rental for a single movie.<br>
 * The rental consists of a movie rented and a period of time in days<br>
 * for which it was rented.
 * 
 * @author Michael Bowes
 */
public class Rental {

	/**
	 * The number of days for the rental period.
	 */
	private int daysRented = 0;
	/**
	 * The rented movie.
	 */
	private Movie movie;
	/**
	 * The discount for the rented movie. Standard is no discount.
	 */
	private double discount = 1.0;

	/**
	 * The customer whom the rental belongs to.
	 */
	private Customer customer;

	public Rental(Customer customer, Movie movie) {
		this.customer = customer;
		this.movie = movie;
	}
	
	public Movie getMovie() {
		return movie;
	}

	/**
	 * Sets the movie if it is not null.
	 * 
	 * @param movie
	 *            - the movie to be set.
	 */
	public void setMovie(Movie movie) {
		if (movie == null) {
			throw new IllegalArgumentException("Movie cannot be empty");			
		}
		this.movie = movie;
	}

	public int getDaysRented() {
		return daysRented;
	}

	/**
	 * Sets the days rented if at least 0.
	 * 
	 * @param daysRented
	 *            - the days the movie has been rented.
	 */
	public void setDaysRented(int daysRented) {
		if (daysRented < 0) {
			throw new IllegalArgumentException("DaysRented cannot be a negative value");			
		}
		this.daysRented = daysRented;
	}

	/**
	 * Returns the total charge for the rental of the movie.<br>
	 * The amount depends on the type of movie that was set and the<br>
	 * number of days rented, as well as the tax and discounts given.
	 * 
	 * @return The total charge for the rental for a given time.
	 */
	public double getCharge() {
		double listsellingprice = movie.getCharge(daysRented) * discount;
		return listsellingprice;
	}

	/**
	 * Calls the {@link Price#getTaxAmount(int, MovieQuality)} method of the
	 * price<br>
	 * and returns the amount of the tax for the rental.
	 * 
	 * @return The total tax for the rental.
	 */
	double getTaxAmount() {
		return movie.getTaxAmount(daysRented);
	}

	/**
	 * Returns the number of frequent renter points earned for this rental.<br>
	 * It depends on the days the movie was rented and its type.
	 * 
	 * @return The number of frequent renter points earned for this rental.
	 */
	public int getFrequentRenterPoints() {
		return movie.getFrequentRenterPoints(daysRented, this, customer);
	}

	public double getDiscount() {
		return discount;
	}

	/**
	 * Sets the discount iff one of the allowed values of<br>
	 * the discount are given.<br>
	 * The allowed values are:<br>
	 * None for 1.0, meaning 0% discount.<br>
	 * Low 0.7, meaning 30% discount.<br>
	 * High 0.5, meaning 50% discount.<br>
	 * 
	 * @param discount
	 *            - The discount given on the final price for the rental.<br>
	 */
	public void setDiscount(Discount discount) {
		if (discount == null) {
			throw new IllegalArgumentException("The given discount mustn't be null");
		}
		switch (discount) {
			case HIGH:
				this.discount = 0.5;
				break;
	
			case LOW:
				this.discount = 0.7;
				break;
	
			case NONE:
				this.discount = 1.0;
				break;
			}
	}

}
