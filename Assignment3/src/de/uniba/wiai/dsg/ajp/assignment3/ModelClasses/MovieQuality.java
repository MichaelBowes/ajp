package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

/**
 * Enum representing the picture quality of a movie.
 * 
 * @author Nicolas Bruch
 */
public enum MovieQuality {
	/**
	 * High Definition Quality.
	 */
	HD,
	/**
	 * Standard Definition Quality.
	 */
	SD;
}
