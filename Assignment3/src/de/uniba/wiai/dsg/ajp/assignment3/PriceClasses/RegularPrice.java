package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Movie;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;

/**
 * This class represents the pricing type for a movie.<br>
 * The price code for this class is 0.
 */
public class RegularPrice extends Price {

	@Override
	public double getCharge(int daysRented, MovieQuality quality) {
		if (quality == null) {
			throw new IllegalArgumentException("The Quality of the Movie mustn't be null");
		}
		double result = 2;
		if (daysRented > 2) {
			result += (daysRented - 2) * 1.5;
		}
		if (quality == MovieQuality.HD) {
			result += 2;
		}
		return result;
	}

	@Override
	public int getFrequentRenterPoints(int daysRented, Rental rental, Customer customer) {
		int renterpoints = 1;
		if (rental.getDiscount() == 0.7 || rental.getDiscount() == 0.5) {
			renterpoints += 1;
		} else {
			return renterpoints;
		}
		return renterpoints;
	}

	@Override
	public int getPriceCode() {
		return Movie.REGULAR;
	}

}
