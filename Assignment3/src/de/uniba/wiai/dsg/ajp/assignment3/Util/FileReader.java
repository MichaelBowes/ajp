package de.uniba.wiai.dsg.ajp.assignment3.Util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

/**
 * This class contains methods for reading data.
 */
public class FileReader {
	
	/**
	 * Reads a file from a given path and returns its content.
	 * @param path - Path of the file to be read.
	 * @return String containing all strings from the file.
	 * @throws IOException If an IOException occurs.
	 */
	public static String read(String path) throws IOException {
		Path filePath = Paths.get(path);
		String result = "";
		try {		
			List<String> lines = Files.readAllLines(filePath);
			Iterator<String> linesIterator = lines.iterator();
			while (linesIterator.hasNext()) {
				result+= linesIterator.next();
				if(linesIterator.hasNext()) {
					result += "\n";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
}
