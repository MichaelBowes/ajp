package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a customer.
 */
public class Customer {

	/**
	 * The name identifying the customer.
	 */
	private String name;

	/**
	 * A list of the {@link Rental rentals} made by the customer.<br>
	 * Each rental represents a rented movie.
	 */
	private List<Rental> rentals = new LinkedList<Rental>();

	/**
	 * Checks whether a customer has already gained 3 frequent renter points from
	 * renting a series.
	 */
	private boolean rentedSeriesOnce = false;

	public Customer(String name) {
		super();
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("The custumer's name cannot be empty or null");			
		}
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("The custumer's name cannot be empty or null");			
		}
		this.name = name;
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	/**
	 * Sets the rentals if they are not null<br>
	 * and not empty.
	 */
	public void setRentals(List<Rental> rentals) {
		if (rentals == null || rentals.size() == 0 || rentals.contains(null)) {
			throw new IllegalArgumentException("Rentals cannot be empty");			
		}
		this.rentals = rentals;
	}

	/**
	 * Returns all {@link Customer#rentals rentals} made by the customer in form of
	 * a string.<br>
	 * Each line of the string consists of the title of the movie<br>
	 * and its price.<br>
	 * The last lines contain the total charge followed by<br>
	 * the total number of frequent renter points earned by all rentals<br>
	 * 
	 * @return A string containing a record of all movies rented by the customer and
	 *         their charge followed by the total charge and frequent renter points.
	 */
	public String statement() {
		String result = "Rental Record for " + getName() + "\n";

		int frequentRenterPoints = 0;
		double tax = 0;
		for (Rental each : this.rentals) {
			frequentRenterPoints += each.getFrequentRenterPoints();
			tax += each.getTaxAmount();
			// show figures for this rental
			result += "\t" + each.getMovie().getTitle() + "\t" + String.valueOf(each.getCharge()) + "\t"
					+ each.getMovie().getQuality().toString() + "\n";
		}

		// add footer lines
		result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points";
		return result;
	}

	/**
	 * Returns all {@link Customer#rentals rentals} made by the customer in html
	 * form.<br>
	 * The format is as follows:<br>
	 * <br>
	 * {@code 
	 * <H1>Rentals for <EM>} <em>CUSTOMER NAME</em> {@code </EM></H1>
	 * <P>
	 * \n}<br>
	 * <em>MOVIE TITLE</em> : <em>MOVIE CHARGE</em> {@code <BR>
	 * \n} <--(For each {@link Customer#rentals rental})<br>
	 * {@code 
	 * <P>
	 * You owe <EM>} <em>TOTAL CHARGE</em> {@code </EM>
	 * <P>
	 * \n}<br>
	 * {@code On this rental you earned <EM>} <em>TOTAL NUMBER OF FREQUENT RENTER
	 * POINTS</em> {@code </EM> frequent renter points
	 * <P>
	 * }<br>
	 * <br>
	 * 
	 * @return A string with html tags containing a record of all movies rented by
	 *         the customer and their charge followed by the total charge and
	 *         frequent renter points.
	 */
	public String htmlStatement() {
		String result = "<H1>Rentals for <EM>" + getName() + "</EM></H1><P>\n";

		for (Rental each : rentals) {
			// show figures for each rental
			result += each.getMovie().getTitle() + ": " + String.valueOf(each.getCharge()) + "<BR>\n";
		}

		// add footer lines
		result += "<P>You owe <EM>" + String.valueOf(getTotalCharge()) + "</EM><P>\n";
		result += "On this rental you earned <EM>" + String.valueOf(getTotalFrequentRenterPoints())
				+ "</EM> frequent renter points<P>";
		return result;
	}

	/**
	 * Returns the price value for all {@link Rental rentals} made by the
	 * customer.<br>
	 * This value depends on the pricing types of the {@link Movie movies} and the
	 * days they<br>
	 * were rented.
	 * 
	 * @return The total charge for all rentals made.<br>
	 */
	double getTotalCharge() {
		double result = 0;

		for (Rental each : rentals) {
			result += each.getCharge();
		}

		return result;
	}

	/**
	 * Return the number of all frequent renter points earned by the customer for
	 * all {@link Rental rentals}.<br>
	 * The number of points depends on the type of the {@link Movie movies} and the
	 * duration they were rented.<br>
	 * 
	 * @return The number of frequent renter points earned for all rentals.
	 */
	int getTotalFrequentRenterPoints() {
		int result = 0;

		for (Rental each : rentals) {
			result += each.getFrequentRenterPoints();
		}

		return result;
	}

	public boolean getRentedSeriesOnce() {
		return rentedSeriesOnce;
	}

	public void setRentedSeriesOnce(boolean rentedseriesonce) {
		this.rentedSeriesOnce = rentedseriesonce;
	}

}
