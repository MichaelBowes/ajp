package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;

/**
 * test stub class for the abstract class Price
 */
public class TestPrice extends Price {

	@Override
	public double getCharge(int daysRented, MovieQuality quality) {
		return 55.55;
	}

	@Override
	public int getPriceCode() {
		return 0;
	}

}
