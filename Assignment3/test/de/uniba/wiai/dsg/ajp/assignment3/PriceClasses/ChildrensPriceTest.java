package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Discount;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Movie;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;

class ChildrensPriceTest {

	private ChildrensPrice price = new ChildrensPrice();
	private Customer customer = new Customer("Steward");
	private Movie movie = new Movie("TEST", 0, MovieQuality.HD);
	private Rental rental = new Rental(customer, movie);
	
	@Test
	void testGetCharge() {
		assertThrows(IllegalArgumentException.class, () -> price.getCharge(-1, MovieQuality.HD));
		assertThrows(IllegalArgumentException.class, () -> price.getCharge(1, null));
		assertEquals(1.5, price.getCharge(0, MovieQuality.SD));
		assertEquals(3.5, price.getCharge(1, MovieQuality.HD));
		assertEquals(6.5, price.getCharge(5, MovieQuality.HD));
	}

	@Test
	void testGetFrequentRenterPoints() {
		rental.setDiscount(Discount.NONE);
		assertEquals(1, price.getFrequentRenterPoints(1, rental, customer));
		rental.setDiscount(Discount.HIGH);
		assertEquals(2, price.getFrequentRenterPoints(1, rental, customer));
	}

	@Test
	void testGetTaxAmount() {
		assertEquals(0.855, price.getTaxAmount(5, MovieQuality.SD));
	}
	
}
