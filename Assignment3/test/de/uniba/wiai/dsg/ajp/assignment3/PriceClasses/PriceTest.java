package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;

class PriceTest {

	private Price price = new TestPrice();
	private static final double DELTA = 1e-15;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testGetTaxAmount() {
		double testValue = 55.55 * 19.0 / 100;
		assertEquals(testValue, price.getTaxAmount(1, MovieQuality.HD), DELTA);
		
		price.setTax(22.5);
		testValue = 55.55 * 22.5 / 100;
		assertEquals(testValue, price.getTaxAmount(1, MovieQuality.HD), DELTA);
	}

}
