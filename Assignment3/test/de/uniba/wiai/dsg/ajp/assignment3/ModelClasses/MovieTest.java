package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Field;

import org.mockito.internal.util.reflection.FieldSetter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.ChildrensPrice;
import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.NewReleasePrice;
import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.Price;
import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.RegularPrice;
import de.uniba.wiai.dsg.ajp.assignment3.PriceClasses.SeriesPrice;

public class MovieTest {
	
	
	private Movie movie= new Movie("Title", 0, MovieQuality.SD);
	private Price price = mock(Price.class);
	private static final String PRICETFIELDNAME = "price";
	private static final double DELTA = 1e-15;
	private static final double TESTVALUE = 22.22;
	private static final int DAYSRENTED = 5;
	private static boolean fieldSet = true;
	
	@BeforeEach
	public void setToStandard() {
		movie.setQuality(MovieQuality.SD);
		movie.setTitle("Title");
		movie.setPriceCode(0);
		try {
			Field field = Movie.class.getDeclaredField(PRICETFIELDNAME);
			FieldSetter.setField(movie, field, price);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException e) {		
			e.printStackTrace();
			fieldSet = false;
		}
	}
	
	@Test
	public void testConstructor() {
		assertThrows(IllegalArgumentException.class, () -> new Movie(null, 0, MovieQuality.SD));
		assertThrows(IllegalArgumentException.class, () -> new Movie("", 0, MovieQuality.SD));
	}
	
	@Test
	public void testSetTitle() {
		assertThrows(IllegalArgumentException.class, () -> movie.setTitle(null));
		assertThrows(IllegalArgumentException.class, () -> movie.setTitle(""));
		movie.setTitle("Test123");
		assertEquals("Test123", movie.getTitle());
	}

	@Test
	public void testSetQuality() {
		assertEquals(MovieQuality.SD, movie.getQuality());
		movie.setQuality(MovieQuality.HD);
		assertEquals(MovieQuality.HD, movie.getQuality());
		assertThrows(IllegalArgumentException.class, () -> movie.setQuality(null));
	}
	
	@Test
	public void testGetCharge() {
		assertTrue(fieldSet, "Field could not be set");
		assertThrows(IllegalArgumentException.class, () -> movie.getCharge(-2));
		when(price.getCharge(5, MovieQuality.SD)).thenReturn(TESTVALUE);
		assertEquals(movie.getCharge(DAYSRENTED), TESTVALUE, DELTA);
	}
	
	
	@Test
	public void testGetTaxAmount() {
		assertTrue(fieldSet, "Field could not be set");
		when(price.getTaxAmount(5, MovieQuality.SD)).thenReturn(TESTVALUE);
		assertEquals(movie.getTaxAmount(5), TESTVALUE, DELTA);
	}
		
	@Test
	public void testSetPriceCode() {
		assertThrows(IllegalArgumentException.class, () -> movie.setPriceCode(-1));
		assertThrows(IllegalArgumentException.class, () -> movie.setPriceCode(4));
		movie.setPriceCode(0);
		assertEquals(movie.getPrice().getClass(), RegularPrice.class);
		assertEquals(movie.getPriceCode(), 0);
		movie.setPriceCode(1);
		assertEquals(movie.getPrice().getClass(), NewReleasePrice.class);
		assertEquals(movie.getPriceCode(), 1);
		movie.setPriceCode(2);
		assertEquals(movie.getPrice().getClass(), ChildrensPrice.class);
		assertEquals(movie.getPriceCode(), 2);
		movie.setPriceCode(3);
		assertEquals(movie.getPrice().getClass(), SeriesPrice.class);
		assertEquals(movie.getPriceCode(), 3);
	}

	@Test
	public void testGetFrequentRenterPoints() {
		assertTrue(fieldSet, "Field could not be set");
		Rental rental = mock(Rental.class);
		Customer customer = mock(Customer.class);
		when(price.getFrequentRenterPoints(DAYSRENTED, rental, customer)).thenReturn(3);
		assertEquals(movie.getFrequentRenterPoints(DAYSRENTED, rental, customer), 3);
	}

}
