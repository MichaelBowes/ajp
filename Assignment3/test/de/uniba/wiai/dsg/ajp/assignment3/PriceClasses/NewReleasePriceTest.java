package de.uniba.wiai.dsg.ajp.assignment3.PriceClasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Customer;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Discount;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Movie;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.MovieQuality;
import de.uniba.wiai.dsg.ajp.assignment3.ModelClasses.Rental;

class NewReleasePriceTest {

	private NewReleasePrice price = new NewReleasePrice();
	private Customer customer = new Customer("Steward");
	private Movie movie = new Movie("TEST", 0, MovieQuality.HD);
	private Rental rental = new Rental(customer, movie);

	@Test
	void testGetCharge() {
		assertThrows(IllegalArgumentException.class, () -> price.getCharge(1, null));
		assertEquals(3, price.getCharge(1, MovieQuality.SD));
		assertEquals(5, price.getCharge(1, MovieQuality.HD));
	}

	@Test
	void testGetFrequentRenterPoints() {
		rental.setDiscount(Discount.NONE);
		assertEquals(2, price.getFrequentRenterPoints(3, rental, customer));
		rental.setDiscount(Discount.HIGH);
		assertEquals(3, price.getFrequentRenterPoints(3, rental, customer));
		rental.setDiscount(Discount.HIGH);
		assertEquals(1, price.getFrequentRenterPoints(1, rental, customer));
	}

	@Test
	void testGetTaxAmount() {
		assertEquals(2.85, price.getTaxAmount(5, MovieQuality.SD));
	}

}
