package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.uniba.wiai.dsg.ajp.assignment3.Util.FileReader;

public class CustomerTest {
	
	private static final String URLTESTFILESTATEMENT = "test/de/uniba/wiai/dsg/ajp/assignment3/ModelClasses/TestDataStatement.txt";
	private static final String URLTESTFILEHTMLSTATEMENT = "test/de/uniba/wiai/dsg/ajp/assignment3/ModelClasses/TestDataHtmlStatement.txt";
	private static final double DELTA = 1e-15;
	private static Customer customer;
	private static Rental rental;
	private static Movie movie;

	@BeforeAll
	public static void setUp() throws Exception {
		customer = new Customer("Steward");	
		rental = mock(Rental.class);
		movie = mock(Movie.class);
		when(rental.getCharge()).thenReturn(5.5);
		when(rental.getMovie()).thenReturn(movie);
		when(rental.getFrequentRenterPoints()).thenReturn(4);
		when(movie.getTitle()).thenReturn("MovieTitel");
		when(movie.getQuality()).thenReturn(MovieQuality.HD);
	}
	
	@BeforeEach
	public void setRentals() {
		List<Rental> rentals = new LinkedList<Rental>();
		rentals.add(rental);
		rentals.add(rental);
		customer.setRentals(rentals);
	}
	
	@Test
	public void testConstructor() {
		assertThrows(IllegalArgumentException.class,() -> new Customer(null));
		assertThrows(IllegalArgumentException.class,() -> new Customer(""));
	}

	@Test
	public void testSetName() { 
		customer.setName("James");
		assertEquals(customer.getName(),"James");
		assertThrows(IllegalArgumentException.class, () -> customer.setName(null));
		assertThrows(IllegalArgumentException.class, () -> customer.setName(""));
	}

	@Test
	public void testSetRentals() {
		assertThrows(IllegalArgumentException.class, () -> customer.setRentals(null));
		List<Rental> rentals = new LinkedList<Rental>();
		assertThrows(IllegalArgumentException.class, () -> customer.setRentals(rentals));
		rentals.add(rental);
		customer.setRentals(rentals);
		assertEquals(1, rentals.size());
		rentals.add(null);
		assertThrows(IllegalArgumentException.class, () -> customer.setRentals(rentals));
	}

	@Test
	public void testStatement() {		
		String testData;
		try {
			testData = FileReader.read(URLTESTFILESTATEMENT);
		} catch (IOException e) {
			e.printStackTrace();
			testData = null;
		}		
		assertNotNull("Testdata could not be read", testData);
		assertEquals(customer.statement(), testData);
	}

	@Test
	public void testHtmlStatement() {
		String testData;
		try {
			testData = FileReader.read(URLTESTFILEHTMLSTATEMENT);
		} catch (IOException e) {
			e.printStackTrace();
			testData = null;
		}		
		assertNotNull("Testdata could not be read", testData);
		assertEquals(customer.htmlStatement(), testData);
	}

	@Test
	public void testGetTotalCharge() {		
		assertEquals( 11.0, customer.getTotalCharge(), DELTA);
	}

	@Test
	public void testGetTotalFrequentRenterPoints() {		
		assertEquals(8, customer.getTotalFrequentRenterPoints());
	}
}
