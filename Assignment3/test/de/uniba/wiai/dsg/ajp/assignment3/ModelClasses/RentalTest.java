package de.uniba.wiai.dsg.ajp.assignment3.ModelClasses;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;
import org.mockito.internal.util.reflection.FieldSetter;
import org.junit.jupiter.api.BeforeEach;

public class RentalTest {

	private Movie movie = mock(Movie.class);
	private static final double DELTA = 1e-15;
	private static final String DISCOUNTFIELDNAME = "discount";
	private static final double HIGHDISCOUNT = 0.5;
	private static final double LOWDISCOUNT = 0.7;
	private static final double TESTVALUE = 333.22;
	private Customer customer = new Customer("Steward");
	private Rental rental = new Rental(customer, movie);
	
	@BeforeEach
	public void setUp() throws Exception {
		rental.setDaysRented(5);
	}

	@Test
	public void testSetMovie() {
		assertThrows(IllegalArgumentException.class, () -> rental.setMovie(null));
	}
	
	@Test
	public void testSetDaysRented() {
		assertThrows(IllegalArgumentException.class, () -> rental.setDaysRented(-1));
	}
	
	@Test
	public void testGetCharge() {
		when(movie.getCharge(5)).thenReturn(TESTVALUE);
		assertEquals(rental.getCharge(), TESTVALUE, DELTA);
		
		boolean fieldSet = true;		
		//setting discount to high
		try {
			Field field = Rental.class.getDeclaredField(DISCOUNTFIELDNAME);
			FieldSetter.setField(rental, field, HIGHDISCOUNT);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException e) {		
			e.printStackTrace();
		    fieldSet = false;
		}
		double highDiscountCharge = TESTVALUE * HIGHDISCOUNT;
		assertTrue(fieldSet, "Field could not be set");
		assertEquals(rental.getCharge(), highDiscountCharge, DELTA);
		
		//setting discount to low
		try {
			Field field = Rental.class.getDeclaredField(DISCOUNTFIELDNAME);
			FieldSetter.setField(rental, field, LOWDISCOUNT);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException e) {		
			e.printStackTrace(); 
		    fieldSet = false;
		}
		double lowDiscountCharge = TESTVALUE * LOWDISCOUNT;
		assertTrue(fieldSet, "Field could not be set");
		assertEquals(rental.getCharge(), lowDiscountCharge, DELTA);
	}

	@Test
	public void testSetDiscount() {
		rental.setDiscount(Discount.HIGH);
		assertEquals(rental.getDiscount(), 0.5, DELTA);
		rental.setDiscount(Discount.LOW);
		assertEquals(rental.getDiscount(), 0.7, DELTA);
		rental.setDiscount(Discount.NONE);
		assertEquals(rental.getDiscount(), 1, DELTA);
		assertThrows(IllegalArgumentException.class,() -> rental.setDiscount(null));
	}

}
