package de.uniba.wiai.dsg.ajp.assignment2.issuetracking.logic;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.IssueTracker;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.ProjectService;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Project;

public class IssueTrackerImpl implements IssueTracker {

	public IssueTrackerImpl() {
		/*
		 * DO NOT CHANGE - required for grading!
		 */
	}

	@Override
	public void validate(String path) throws IssueTrackingException {
		
		if(path == null){
			throw new IssueTrackingException("Path can't be null", new IllegalArgumentException());
		}

		// creating SchemaFactory und Schema 
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		
		try {
			Schema schema = sf.newSchema(new File("src/de/uniba/wiai/dsg/ajp/assignment2/issuetracking/model/schema1.xsd"));
			// creating Validator object zur Validierung 
			Validator validator = schema.newValidator();
	
			validator.validate(new StreamSource(path));
		} catch (SAXException | IOException e) {
			throw new IssueTrackingException("Exception: ", e);
		}
	}

	@Override
	public ProjectService load(String path) throws IssueTrackingException {

		if(path == null){
			throw new IssueTrackingException("Path can't be null", new IllegalArgumentException());
		}
		
		Project p;
		
		try{
		    //getting the xml file to read
		    File file = new File(path);
		    
		    //creating the JAXB context
		    JAXBContext jContext = JAXBContext.newInstance(Project.class);
		    
		    //creating the unmarshall object
		    Unmarshaller unmarshallerObj = jContext.createUnmarshaller();
		    
		    // validing the path
			this.validate(path);
			
		    //calling the unmarshall method
		    p = (Project) unmarshallerObj.unmarshal(file);
		    
		} catch (JAXBException e) {
			throw new IssueTrackingException("Could not load XML file", e);
		}
		
		ProjectService ps = new ProjectServiceImpl(p);
		
		return ps;
	}

	@Override
	public ProjectService create() {

		ProjectService ps = new ProjectServiceImpl();
		
		return ps;
	}

	

}
