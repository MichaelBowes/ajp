package de.uniba.wiai.dsg.ajp.assignment2;

import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.view.*;

public class Main {

	
	
	public static void main(String[] args) {
		ConsoleHelper console = ConsoleHelper.build();
		ConsoleDisplay display = new ConsoleDisplay(console, console.getOut());
		
		display.displayMainMenu();
	}
	
}
