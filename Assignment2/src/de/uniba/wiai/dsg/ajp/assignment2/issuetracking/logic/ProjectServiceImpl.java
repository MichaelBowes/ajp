package de.uniba.wiai.dsg.ajp.assignment2.issuetracking.logic;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.ProjectService;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Issue;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Milestone;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Project;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Severity;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.State;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Type;

public class ProjectServiceImpl implements ProjectService {

	private Project project;
	
	public ProjectServiceImpl(){
		project = new Project();
	}
	
	public ProjectServiceImpl(Project project){
		this.project = project;
	}
	
	@Override
	public void createMilestone(String id, String name, Set<String> issues)
			throws IssueTrackingException {
		Milestone milestone = new Milestone();
		
		//Check arguments
		if(!ValidationHelper.isId(id)){
			throw new IssueTrackingException("Milestone ID is invalid", new IllegalArgumentException());		
		}
		if(idExists(id)){
			throw new IssueTrackingException("ID already exists", new IllegalArgumentException());
		}
		if(name == null){
			throw new IssueTrackingException("Milestone name is invalid", new IllegalArgumentException());
		}
		if(issues == null){
			throw new IssueTrackingException("Milestone issues are invalid", new IllegalArgumentException());
		}
		
		milestone.setId(id);
		milestone.setName(name);
		
		List<Issue> issueList = new LinkedList<Issue>();
		for(String issueId : issues){
			Issue issue = getIssue(issueId);
			//Check if the issue exists
			if(issue != null){
				issueList.add(issue);				
			}else{
				throw new IssueTrackingException("Issue with id "+ issueId +" does not exist");	
			}
		}
		milestone.setIssues(issueList);	
		
		project.getMilestones().add(milestone);
	}

	@Override
	public List<Milestone> getMilestones() {
		return project.getMilestones();
	}

	@Override
	public void removeMilestone(String id) throws IssueTrackingException {
		Milestone tempMilestone = null;
		for(Milestone milestone : project.getMilestones()){
			if(milestone.getId().equals(id))
				tempMilestone = milestone;	
		}
		if(tempMilestone == null){
			throw new IssueTrackingException("Milestone not found");			
		}else{
			project.getMilestones().remove(tempMilestone);
		}
	}

	@Override
	public void createIssue(String id, String name, String description,
			Severity severity, Type type, String milestone,
			Set<String> dependencies) throws IssueTrackingException {
		Issue issue = new Issue();
		
		//Check arguments
		if(!ValidationHelper.isId(id)){
			throw new IssueTrackingException("Issue ID is invalid", new IllegalArgumentException());				
		}				
		if(idExists(id)){
			throw new IssueTrackingException("ID already exists", new IllegalArgumentException());
		}
		if(name == null){
			throw new IssueTrackingException("Issue name is invalid", new IllegalArgumentException());
		}
		if(severity == null){
			throw new IssueTrackingException("Issue severety is invalid", new IllegalArgumentException());
		}
		if(type == null){
			throw new IssueTrackingException("Issue type is invalid", new IllegalArgumentException());
		}
		if(description == null){
			throw new IssueTrackingException("Issue description is invalid", new IllegalArgumentException());
		}
		if(milestone == null){
			throw new IssueTrackingException("Issue milestone is invalid", new IllegalArgumentException());
		}
		if(dependencies == null){
			throw new IssueTrackingException("Issue dependencies are invalid", new IllegalArgumentException());
		}
		
		issue.setId(id);
		issue.setName(name);		
		issue.setDescription(description);
		issue.setType(type);
		issue.setSeverity(severity);
		issue.setState(State.OPEN);
		
		//Set Milestone
		if(!milestone.equals("")){
			Milestone foundMilestone = getMilestone(milestone);
			//Check if milestone exists
			if(foundMilestone == null){
				throw new IssueTrackingException("Given Milestone for Issue was not found.");
			}else{
				issue.setMilestone(foundMilestone);
				//Add issue to milestone
				foundMilestone.getIssues().add(issue); 
			}
		}
		
		//Set Dependencies
		List<Issue> dependencyList = new LinkedList<Issue>();
		for(String depIssueId : dependencies){
			Issue depIssue = getIssue(depIssueId);
			//Check if issue exists
			if(depIssue == null){
				throw new IssueTrackingException("Given dependency for Issue was invalid",
						new IllegalArgumentException());
			}else{
				dependencyList.add(depIssue);
			}
		}
		issue.setDependencies(dependencyList);		
		
		project.getIssues().add(issue);
	}

	@Override
	public List<Issue> getIssues() {
		return project.getIssues();
	}

	@Override
	public void removeIssue(String id) throws IssueTrackingException {
		if(id == null){
			throw new IssueTrackingException("Id can't be null", new IllegalArgumentException());
		}
		//delete issue from milestones
		for(Milestone milestone : project.getMilestones()){
			milestone.getIssues().removeIf(i -> i.getId().equals(id));
		}
		//Delete issue from dependencies
		for(Issue issue : project.getIssues()){
			issue.getDependencies().removeIf(i -> i.getId().equals(id));
		}
		project.getIssues().removeIf(i -> i.getId().equals(id));
	}

	@Override
	public void closeIssue(String id) throws IssueTrackingException {
		Issue issue = getIssue(id);
		if(issue == null){
			throw new IssueTrackingException("Issue id not found");
		}
		
		//Check if dependent issues are closed
		boolean issuesClosed = true;
		for(Issue depIssue : issue.getDependencies()){
			if(depIssue.getState() == State.OPEN){
				issuesClosed = false;
			}
		}
		if(issuesClosed){
			issue.setState(State.CLOSED);
		}else{
			throw new IssueTrackingException("Dependent issue is still open");
		}
	}

	@Override
	public void printXMLToConsole() throws IssueTrackingException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class, Issue.class, Milestone.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			//set formating
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			//marshal to output
			jaxbMarshaller.marshal(this.project, System.out);
			
		} catch (JAXBException e) {
			throw new IssueTrackingException("Could not print XML to console", e);
		}

	}

	@Override
	public void saveXMLToFile(String path) throws IssueTrackingException {
		if(path == null){
			throw new IssueTrackingException("Path can't be null", new IllegalArgumentException());
		}
		
		try {
			File file = new File(path);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class, Issue.class, Milestone.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			//set formating
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			//marshal to output
			jaxbMarshaller.marshal(this.project, file);
			
		} catch (JAXBException e) {
			throw new IssueTrackingException("Could not save XML to file", e);
		}
	}
	
	/**
	 * Returns a Milestone for a given ID.
	 * @return A Milestone for a given id.
	 * If the Milestone does not exists, null is returned.
	 */
	private Milestone getMilestone(String id){
		for(Milestone milestone : project.getMilestones()){
			if(milestone.getId().equals(id)){
				return milestone;
			}
		}
		return null;
	}
	/**
	 * Returns a Issue for a given ID.
	 * @return A Issue for a given id.
	 * If the Issue does not exists, null is returned.
	 */
	private Issue getIssue(String id){
		for(Issue issue : project.getIssues()){
			if(issue.getId().equals(id)){
				return issue;
			}
		}
		return null;
	}
	
	/**
	 * Checks if a given id already exists within
	 * the project.
	 * @param id to be checked.
	 * @return True if the id already exists
	 */
	private boolean idExists(String id){
		for(Milestone milestone : project.getMilestones()){
			if(milestone.getId().equals(id)){
				return true;
			}
		}
		for(Issue issue : project.getIssues()){
			if(issue.getId().equals(id)){
				return true;
			}
		}
		return false;
	}
}
