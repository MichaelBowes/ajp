package de.uniba.wiai.dsg.ajp.assignment2.issuetracking.view;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.IssueTracker;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.ProjectService;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.logic.IssueTrackerImpl;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Issue;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Milestone;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Severity;
import de.uniba.wiai.dsg.ajp.assignment2.issuetracking.model.Type;

/**
 * @author Nicolas Bruch
 * 
 * This class contains methods for displaying the interfaces for the user
 * to the console.
 *
 */
public class ConsoleDisplay {

	private PrintStream out;
	private ConsoleHelper console;
	
	public ConsoleDisplay(ConsoleHelper console, PrintStream out){
		this.out = out;
		this.console = console;
	}
	
	/**
	 * Displays the main menu
	 */
	public void displayMainMenu() {
		boolean exit = false;
		while(!exit){
			//Display menu
			out.println("( 1 ) Validate and Load Project");
			out.println("( 2 ) Create New Project");
			out.println("( 0 ) Exit");
			
			int input = 0;			
			 try {			
				input = console.askIntegerInRange("Please choose an option", 0, 2);	
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			 
			//handle the user input
			boolean submenuAccess = false;
			ProjectService projectService = null;
			IssueTracker issueTracker = new IssueTrackerImpl();
			
			switch(input){
				case 0:
					exit = true;
					break;
				
				case 1:
					String inputPath = null;
					try {
						inputPath = console.askString("Enter a file path");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					try {
						projectService = issueTracker.load(inputPath);
						submenuAccess = true;
					} catch (IssueTrackingException e) {
						out.println("Could not load file!");
						out.println(e.getMessage());
						if(e.getCause()!= null){
							out.println(e.getCause().getMessage());							
						}
						submenuAccess = false;
					}
					break;
					
				case 2:
					projectService = issueTracker.create();
					submenuAccess = true;
					break;			
			}
			
			if(submenuAccess){
				displaySubmenu(projectService);
			}
		}		 
	}
	
	
	/**
	 * Displays the submenu.
	 * @param projectService
	 */
	private void displaySubmenu(ProjectService projectService) {		
		boolean exit = false;
		while(!exit){
			//Display menu
			out.println("( 1 ) Add Milestone");
			out.println("( 2 ) Remove Milestone and Cleanup");
			out.println("( 3 ) List Milestones");
			out.println("( 4 ) Add Issue");
			out.println("( 5 ) Close Issue");
			out.println("( 6 ) Remove Issue and Cleanup");
			out.println("( 7 ) List Issues");
			out.println("( 8 ) Print XML on Console");
			out.println("( 9 ) Save XML to File");
			out.println("( 0 ) Back to main menu / close without saving");
			
			
			int input = -1;
			try {
				input = console.askIntegerInRange("Please choose an option", 0, 9);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			
			//Handle user input
			String id = "";
			String name = "";
			String description = "";
			Severity severity = null;
			Type type = null;
			Set<String> idList = new HashSet<String>();
			switch(input){
				case 0: //Back to main menu / close without saving
					exit = true;
					break;
					
				case 1: //Add Milestone										
					try {
						id = console.askNonEmptyString("Please enter an id");
						name = console.askNonEmptyString("Please enter a name");
						if(getConfirmation("Do you want to add issues to the milestone?")){
							idList = getStringSet();							
						}
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					try {
						projectService.createMilestone(id, name, idList);
						out.println("Milestone with id '" + id + "' was successfully created.");
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 2: //Remove Milestone and Cleanup
					try {
						id = console.askNonEmptyString("Please enter an id");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					try {
						projectService.removeMilestone(id);
						out.println("Milestone with id '" + id + "' was successfully removed.");
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 3: //List Milestones
					out.println("Milestones found: " + projectService.getMilestones().size());
					for(Milestone milestone : projectService.getMilestones()){
						out.println(milestone.toString());					
					}
					break;
					
				case 4: //Add Issue
					String milestoneID = "";
					try {
						id = console.askNonEmptyString("Please enter an id");
						name = console.askNonEmptyString("Please enter a name");
						description = console.askString("Please enter a description (can be left empty)");
						severity = chooseSeverity();
						if(severity == null){
							break;
						}
						type = chooseType();
						if(type == null){
							break;
						}
						//Ask for milestone
						if(getConfirmation("Does this issue belong to a milestone?")){
							milestoneID = console.askNonEmptyString("Please enter the milestone id.");
						}
						//Ask for dependencies
						if(getConfirmation("Do you want to add dependencies?")){
							idList = getStringSet();							
						}
					} catch (IOException e) {
						throw new RuntimeException(e);
					}				
						
					try {
						projectService.createIssue(id, name, description, severity, type, milestoneID, idList);
						out.println("Issue with id '" + id + "' was successfully created.");
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 5: //Close Issue
					try {
						id = console.askNonEmptyString("Please enter an id");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					try {
						projectService.closeIssue(id);
						out.println("Issue with id '" + id + "' was closed.");
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 6: //Remove Issue and Cleanup
					try {
						id = console.askNonEmptyString("Please enter an id");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					try {
						projectService.removeIssue(id);
						out.println("Issue with id '" + id + "' was removed.");
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 7: //List Issues
					out.println("Issues found: " + projectService.getIssues().size());
					for(Issue issue : projectService.getIssues()){
						out.println(issue.toString());					
					}
					break;
				
				case 8: //Print XML on Console
					try {
						projectService.printXMLToConsole();
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
					
				case 9: //Save XML to File
					String path = null;
					try {
						path = console.askNonEmptyString("Please enter a file path");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					
					try {
						projectService.saveXMLToFile(path);
						out.println("File was successfully created in path:");
						out.println(path);
					} catch (IssueTrackingException e) {
						out.println(e.getMessage());
					}
					break;
			}
		}	
		
	}
	
	/**
	 * Lets the user choose between the different severities of issues.
	 * @return the chosen Severity for an Issue.
	 * @throws IOException
	 */
	private Severity chooseSeverity() throws IOException{
		Severity result = null;
		//Options
		out.println("( 1 ) Trivial");
		out.println("( 2 ) Minor");
		out.println("( 3 ) Major");
		out.println("( 4 ) Critical");
		out.println("( 0 ) Cancel");
			
		int input = -1;	
		input = console.askIntegerInRange("Please choose the severity level", 0, 4);
		
		switch(input){
			case 0:
				break;
				
			case 1:
				result = Severity.TRIVIAL;
				break;
				
			case 2:
				result = Severity.MINOR;
				break;
				
			case 3:
				result = Severity.MAJOR;
				break;

			case 4:
				result = Severity.CRITICAL;
				break;
				
		}	
		return result;
	}
	
	/**
	 * Lets the user choose between the different types of issues.
	 * @return the chosen Type for an Issue.
	 * @throws IOException
	 */
	private Type chooseType() throws IOException{
		Type result = null;
		//Options
		out.println("( 1 ) Bug");
		out.println("( 2 ) Feature");
		out.println("( 0 ) Cancel");
		
		int input = -1;
		input = console.askIntegerInRange("Please choose an option", 0, 2);

		switch(input){
			case 0:
				break;
				
			case 1:
				result = Type.BUG;
				break;
				
			case 2:
				result = Type.FEATURE;
				break;
				
		}
		return result;
	}
	
	/**
	 * Lets the user enter a multiple strings.
	 * @return a list of entered strings
	 * @throws IOException
	 */
	private Set<String> getStringSet() throws IOException{
		String input = "DEFAULT";
		Set<String> resultSet = new HashSet<String>();
		while(!input.equals("")){	
			input = console.askString("Enter an id to add (enter an empty string to cancel)");
			if(!input.equals("")){
				resultSet.add(input);				
			}
		}
		return resultSet;
	}
	
	/**
	 * gets confirmation from the user.
	 * @param message
	 * @return
	 */
	private boolean getConfirmation(String message){
		boolean result = false;
		int input = -1;
		out.println(message);	
		out.println("( 1 ) Yes");
		out.println("( 2 ) No");
		
		try {
			input = console.askIntegerInRange("Please choose an option", 1, 2);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		switch(input){
			case 1:
				result = true;
				break;
			
			case 2:
				result = false;
				break;
				
			default:
				result = false;
				break;
		}
		return result;
	}
	
}
