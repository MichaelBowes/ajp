package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view.IssueDescriptionView;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import javafx.stage.Modality;

public class IssueDescriptionController {
    
    private IssueDescriptionView view;
    private final Issue issue;
    
    public IssueDescriptionController(Issue issue) {
        this.issue = issue;
    }
    
    public void createView(){
        view = new IssueDescriptionView(this, issue);
        view.initModality(Modality.APPLICATION_MODAL);
        view.showAndWait();
    }
    
    public void closeView() {
        view.close();
    }
    
}
