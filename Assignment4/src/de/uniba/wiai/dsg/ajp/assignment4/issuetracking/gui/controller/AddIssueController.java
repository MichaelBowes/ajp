package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller;

import java.util.Set;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model.MainModelImpl;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view.AddIssueView;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Severity;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Type;
import javafx.stage.Modality;

/**
 * 
 * The controller for the AddIssueView.<br>
 * It changes the {@link MainModelImpl} and the {@link AddIssueView}<br>
 * with the input it receives from the {@link AddIssueView}.
 * 
 */
public class AddIssueController {

	private final MainModelImpl model;
	private AddIssueView view;
	
	public AddIssueController(MainModelImpl model) {
		this.model = model;
	}
	
	/**
	 * 
	 * A call to the model to add an issue with the given parameters.
	 * 
	 * @param id The ID of the issue to be added.
	 * @param name The name of the issue to be added.
	 * @param description The description of the issue to be added.
	 * @param severity The severity of the issue to be added.
	 * @param type The type of the issue to be added.
	 * @param dependencies The dependencies of the issue to be added.
	 */
	public void addIssue(String id, String name, String description,
			Severity severity, Type type, Set<String> dependencies){
		if(type == null) {
			view.showErrorMessage("Typ darf nicht leer sein.");
			return;
		}
		if(severity == null) {
			view.showErrorMessage("Schwere darf nicht leer sein.");
			return;
		}		
		try {
				model.addIssue(id, name, description, 
						severity, type, dependencies);
				view.close();
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}
	
	/**
	 * 
	 * Closes the selected issue if called upon.
	 */
	public void closeSelectedIssue() {
		try {
			model.closeSelectedIssue();
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}
	
	/**
	 * Initializes the {@link AddIssueView} that this controller interacts with.
	 */
	public void createView(){
		view = new AddIssueView(this, model);
		view.initModality(Modality.APPLICATION_MODAL);
		view.showAndWait();
	}
	
	/**
	 * Closes the view if called upon.
	 */
	public void closeView(){
		view.close();
	}
}