package de.uniba.wiai.dsg.ajp.assignment4;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.MainController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model.MainModelImpl;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void run(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		MainModelImpl model = new MainModelImpl();
		MainController controller = new MainController(model);	
		controller.startUp();
	}

}
