package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.IssueDescriptionController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class IssueDescriptionView extends Stage {
    
    private final IssueDescriptionController controller;
    private final Issue issue;
    private TextArea descrField;
    
    public IssueDescriptionView(IssueDescriptionController controller, Issue issue) {
        this.issue = issue;
        this.controller = controller;
        
        Pane rootPane = createRootPane();
        
        this.setTitle("Beschreibung von Issue " + issue.getId() + " / " + issue.getName());
        // Setting the scene
        this.setScene(new Scene(rootPane, 400, 500));
        
        this.setMinWidth(400);
        this.setMinHeight(500);
    }
    
    private Pane createRootPane() {
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.centerProperty();
        
        // Setting elements of the root pane
        root.setCenter(createOutputPane());
        root.setBottom(createBackButtonPane());
        
        root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        return root;
    }
    
    private Node createOutputPane() {
        HBox box = new HBox();
        
        BorderStroke stroke = new BorderStroke(Color.GREY, BorderStrokeStyle.SOLID,
        		new CornerRadii(5), BorderWidths.DEFAULT);     
        
        descrField = new TextArea(issue.getDescription());       
        descrField.setWrapText(true);
        descrField.setEditable(false);
        HBox.setHgrow(descrField, Priority.ALWAYS);
        box.setBorder(new Border(stroke));
        box.setPadding(new Insets(10, 10, 10, 10));
        box.getChildren().add(descrField);
        
        return box;
    }
    
    private Node createBackButtonPane() {
        
        Button backButton = new Button("Zurück");
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                controller.closeView();
            }
        });
        
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().add(backButton);
        AnchorPane.setRightAnchor(backButton, 10.0);
        return anchorPane;
    }
    
    public void showErrorMessage(String string){
        Alert alert = new Alert(AlertType.ERROR, string);
        alert.showAndWait();
    }
}
