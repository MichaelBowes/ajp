package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model;

import java.util.Set;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.IssueTracker;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.ProjectService;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Severity;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Type;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.impl.IssueTrackerImpl;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.impl.ProjectServiceImpl;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;


public class MainModelImpl extends MainModel {

	/**
	 * Currently loaded Project.
	 */
	private ProjectService loadedProject;
	/**
	 * Absolute path of the loaded project.
	 */
	private SimpleStringProperty projectPath;
	/**
	 * List containing all issues from the currently loaded project.
	 */
	private SimpleListProperty<Issue> issues;

	private Issue selectedIssue;
	
	private IssueTracker issueTracker;	
	
	public MainModelImpl() {
		projectPath = new SimpleStringProperty("Neues Issue Tracking Projekt");
		issues = new SimpleListProperty<Issue>(FXCollections.observableArrayList());
	}
	
	
	/**
	 * Initializes the model.
	 * @throws IssueTrackingException If the IssueTracker could not be initialized.
	 */
	public void initialize() throws IssueTrackingException {
		issueTracker = new IssueTrackerImpl();
	}
	
	@Override
	public boolean isProjectLoaded() {
		if(loadedProject == null) {
			return false;
		}
		return true;
	}
	
	@Override
	public SimpleListProperty<Issue> getIssueListProperty(){
		return issues;
	}
		
	@Override
	public void setProject(ProjectService project, String projectPath) {
		if(project != null) {
			this.loadedProject = project;
			this.issues.set(FXCollections.observableArrayList(project.getIssues()));
			this.projectPath.set(projectPath);
		}else {
			this.loadedProject = null;
			this.projectPath.set(null);
			this.selectedIssue = null;
			this.issues.clear();
		}	
	}
	
	private void updateIssues() {
		if(loadedProject != null) {
			this.issues.clear();
			this.issues.set(FXCollections.observableArrayList(loadedProject.getIssues()));			
		}
	}
	
	@Override
	public SimpleStringProperty getProjectPath() {
		if(projectPath.getValue() == null) {
			this.projectPath.setValue("Neues Issue Tracking Projekt");
			return projectPath;
		}else {
			return projectPath;
		}
	}
	
	@Override
	public void setSelectedIssue(Issue issue) {
		selectedIssue = issue;
	}
	
	@Override
	public Issue getSelectedIssue() {
		return selectedIssue;
	}
	
	@Override
	public void addIssue(String id, String name, String description,
			Severity severity, Type type, Set<String> dependencies) throws IssueTrackingException {
		loadedProject.createIssue(id, name, description, severity, type, dependencies);
		updateIssues();		
	}
	
	@Override
	public void deleteSelectedIssue() throws IssueTrackingException {
		if(selectedIssue != null) {
			loadedProject.removeIssue(selectedIssue.getId());	
			if(issues.remove(selectedIssue)) {
				selectedIssue = null;
			}
		}
	}
	
	@Override
	public void closeSelectedIssue() throws IssueTrackingException {
		if(selectedIssue != null) {
			loadedProject.closeIssue(selectedIssue.getId());
			updateIssues();
		}
	}
	
	@Override
	public boolean saveProject() throws IssueTrackingException {
		if(projectPath.getValue() != null && loadedProject != null) {
			loadedProject.saveXMLToFile(projectPath.getValue());	
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public void saveProject(String path) throws IssueTrackingException {
		loadedProject.saveXMLToFile(path);
	}
	
	@Override
	public void loadProject(String path) throws IssueTrackingException {
		setProject(issueTracker.load(path), path);
	}
	
	@Override
	public void newProject() {
		/*
		}
		try {
			issueTracker = new IssueTrackerImpl();
		} catch (IssueTrackingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		issueTracker.create();*/
		setProject(null, null);
	}
	
}
