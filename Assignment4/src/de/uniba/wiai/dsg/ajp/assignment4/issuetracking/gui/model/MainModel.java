package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model;

import java.util.Set;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.IssueTrackingException;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Project;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.ProjectService;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Severity;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.State;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Type;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;

public abstract class MainModel {

	public abstract void initialize() throws IssueTrackingException;
	
	/**
	 * Checks if a project is currently loaded.
	 * @return {@link True} if a project is loaded.<br>
	 * Otherwise returns {@link False}.
	 */
	public abstract boolean isProjectLoaded();
	
	/**
	 * Returns the {@link Issue}s of the currently loaded {@link Project}.<br>
	 * If no project is loaded a empty list will be returned instead.
	 * @return All issues of the currently loaded {@link Project}.
	 */
	public abstract SimpleListProperty<Issue> getIssueListProperty();
	
	/**
	 * Sets a project as loaded project.<br>
	 * Also changed the Issues and project path.
	 * @param project - Project to be set as loaded project.
	 * @param projectPath - Absolute path of the loaded project.
	 */
	public abstract void setProject(ProjectService project, String projectPath);
	
	/**
	 * Returns the absolute path of the project as {@link SimpleStringProperty}.
	 * @return The absolute path of the project as {@link SimpleStringProperty}.<br>
	 * If no project is loaded, the string "Neues Issue Tracking Projekt" will be returned instead.
	 */
	public abstract SimpleStringProperty getProjectPath();
	
	public abstract void setSelectedIssue(Issue issue);
	
	public abstract Issue getSelectedIssue();
	
	/**
	 * Creates a new {@link Issue}. New {@link Issue}s are in <code>OPEN</code>
	 * {@link State}.
	 * 
	 * @param id
	 *            the ID of the new {@link Issue}. Must not be <code>null</code>
	 *            or empty. Must be a valid ID and unique within the current
	 *            project.
	 * @param name
	 *            the name of the new {@link Issue} is a headline to the
	 *            description. Must not be <code>null</code> or empty.
	 * @param description
	 *            a description of the new {@link Issue} shows the
	 *            problem/feature in such Detail, that it can be implemented by
	 *            those who read it. Must not be <code>null</code>, but may be
	 *            empty.
	 * @param severity
	 *            the severity of the new {@link Issue}.
	 * @param type
	 *            the {@link Issue} tells whether to fix a problem or to
	 *            implement something new.
	 * @param dependencies
	 *            optional relationship that indicates the {@link Issue}s that
	 *            need to be closed, so the new {@link Issue} can be closed as
	 *            well. The dependent {@link Issue}s are represented by their
	 *            IDs. Must not be <code>null</code> and the IDs of all
	 *            {@link Issue}s need to exist and must not be <code>null</code>
	 *            or empty. The list may be empty.
	 * @throws IssueTrackingException
	 *             if the parameters are invalid.
	 */
	public abstract void addIssue(String id, String name, String description,
			Severity severity, Type type, Set<String> dependencies) throws IssueTrackingException;
	
	/**
	 * Deletes an {@link Issue} by ID of the selected {@link Issue}. Also deletes all references to this
	 * {@link Issue} in other {@link Issue}s.
	 * 
	 * @throws IssueTrackingException
	 *             {@link Issue} with the ID of the selected {@link Issue} does not exist in project or one
	 *             of the dependent {@link Issue}s caused a problem during the
	 *             deletion of the references.
	 */
	public abstract void deleteSelectedIssue() throws IssueTrackingException;
	
	/**
	 * Closes an {@link Issue} by id of the selected {@link Issue}. An {@link Issue} that depends on other
	 * open {@link Issue}s cannot be closed.
	 * 
	 * @throws IssueTrackingException
	 *             {@link Issue} with the ID of the selected {@link Issue} does not exist in project or one
	 *             of the dependent {@link Issue}s is not closed.
	 */
	public abstract void closeSelectedIssue() throws IssueTrackingException;
	
	/**
	 * Saves the project to its existing path.
	 * @return 
	 * 		{@link True} if the file could be saved.<br>
	 * 		{@link False} if the file could not be saved.
	 * 
	 * @throws IssueTrackingException
	 * 		If and error occured while writing the file.
	 */
	public abstract boolean saveProject() throws IssueTrackingException;
	
	/**
	 * Saves the project to a given path.
	 * @param path - Path in which the project is to be saved.
	 * @throws IssueTrackingException
	 * 		If an error occurs while writing the file.
	 */
	public abstract void saveProject(String path) throws IssueTrackingException;
	
	/**
	 * Loads a project from a given path and sets it as loaded project.
	 * @param path of the project to be loaded.
	 * @throws IssueTrackingException
	 * 		If an error occured while reading the file.<br>
	 * 		If the file could not be validated.<br>
	 */
	public abstract void loadProject(String path) throws IssueTrackingException;
	
	/**
	 * Sets the loaded Project and the project path to {@link Null}.<br>
	 * The list of {@link Issue}s is cleared.
	 */
	public abstract void newProject();
}
