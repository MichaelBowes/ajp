package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model.MainModelImpl;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view.MainView;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.IssueTrackingException;

public class MainController {

	private final MainModelImpl model;
	private MainView view;
	
	public MainController(MainModelImpl model) {
		this.model = model;
	}
	
	public void startUp(){
		view = new MainView(model, this);
		try {
			model.initialize();
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
			view.close();
		}
		view.showAndWait();
	}	
	
	public boolean endApplicationDialog() {
		boolean result = view.showConfirmationAlert("Are you sure you want to close the application?");
		if(result) {
			view.close();
		}
		return result;
	}
	
	public void createNewProject() {
		model.newProject();
	}
	
	public void loadProjectDialog() {
		String path = view.showFileDialog();
		if(path != null) {
			try {
				model.loadProject(path);
			} catch (IssueTrackingException e) {
				view.showErrorMessage(e.getMessage());
			}
		}else {
			view.showErrorMessage("Invalid file path");
		}			
	}
	
	public void saveProject() {
		try {
			if(!model.saveProject()) {
				view.showErrorMessage("Could not save project");
			}	
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}
	
	public void saveProjectDialog() {
		String path = view.showFileDialog();
		if(path != null && model.isProjectLoaded()) {
			try {
				model.saveProject(path);
			} catch (IssueTrackingException e) {
				view.showErrorMessage(e.getMessage());
			}
		}else {
			view.showErrorMessage("Invalid file path");
		}
	}
	
	public void displayIssueDetails(Issue issue) {
		if(issue != null) {
			view.showIssueDiscription(issue);
		}
	}
	
	public void deleteSelectedIssue() {
		try {
			model.deleteSelectedIssue();
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}
	
	public void closeSelectedIssue() {
		try {
			model.closeSelectedIssue();
		} catch (IssueTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}
	
	public void addIssueDialog() {
		if(model.isProjectLoaded()) {
			view.showAddIssueView();			
		}else {
			view.showInfoMessage("You can't add an issue if no project is loaded");
		}
	}
}
