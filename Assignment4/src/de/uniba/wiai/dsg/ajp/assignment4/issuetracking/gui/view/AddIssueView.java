package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.AddIssueController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model.MainModelImpl;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Severity;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Type;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * 
 * The view for the window where issues can be added to the project.<br>
 * Operates with the {@link MainModelImpl} and its specific
 * {@link AddIssueController}.
 *
 */

public class AddIssueView extends Stage {

	private final AddIssueController controller;
	private final MainModelImpl model;

	/**
	 * A {@link TextField} that expects the user to input an id for the issue to
	 * add.
	 */
	private TextField idField;
	/**
	 * A {@link TextField} that expects the user to input an name for the issue
	 * to add.
	 */
	private TextField nameField;
	/**
	 * A {@link TextField} that expects the user to input an description for the
	 * issue to add.
	 */
	private TextArea descrField;
	/**
	 * A {@link ListView} of Strings that expects the user to select issues
	 * dependent on the issue to be added.
	 */
	private ListView<String> dependencies;
	/**
	 * A {@link ComboBox} of {@link Severity}s that expects the user to select a
	 * severity for the issue to be added.
	 */
	private ComboBox<Severity> comboBoxSeverity;
	/**
	 * A {@link ComboBox} of {@link Type}s that expects the user to select a
	 * type for the issue to be added.
	 */
	private ComboBox<Type> comboBoxType;

	/**
	 * Constructor for the {@link AddIssueView} initializes the scene, sets the
	 * stage's title and adds a handler for close requests of the window.
	 * 
	 * @param controller
	 *            The controller for the AddIssueView.
	 * @param model
	 *            The main model for all views.
	 * 
	 */
	public AddIssueView(AddIssueController controller, MainModelImpl model) {
		this.controller = controller;
		this.model = model;
		Pane rootPane = createRootPane();
		// Adding the window title
		this.setTitle("Issue hinzufügen");
		// Setting the scene
		this.setScene(new Scene(rootPane, 400, 575));
		this.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent event) {
				controller.closeView();
			}
		});
		this.setMinWidth(450);
		this.setMinHeight(650);
	}

	/**
	 * Internal method that creates the main pane with content to be displayed.
	 * 
	 * @return the {@link BorderPane} that serves as root for all other panes.
	 */
	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		root.setPadding(new Insets(10, 0, 0, 10));

		// Setting elements of the root pane
		root.setCenter(createInputPane());
		root.setBottom(createButtonPane());

		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		return root;
	}

	/**
	 * Internal method that initializes the input fields<br>
	 * for the user to provide the required information to add<br>
	 * an issue.
	 * 
	 * @return the {@link AnchorPane} that contains the user input fields.
	 */
	private Node createInputPane() {
		AnchorPane anchorPane = new AnchorPane();
		GridPane gridPane = new GridPane();
		gridPane.setBorder(new Border(
				new BorderStroke(Color.GREY, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
		gridPane.setPadding(new Insets(20, 20, 20, 20));
		gridPane.setVgap(25);
		gridPane.setHgap(25);
		Label idLabel = new Label("ID:");
		Label nameLabel = new Label("Name:");
		Label descrLabel = new Label("Beschreibung:");
		Label severLabel = new Label("Schwere:");
		Label typeLabel = new Label("Typ:");
		Label dependLabel = new Label("Abhängige Issues:");
		idField = new TextField();
		nameField = new TextField();
		descrField = new TextArea();
		dependencies = new ListView<String>();
		ObservableList<Issue> issues = model.getIssueListProperty().getValue();
		ObservableList<String> data = FXCollections.observableArrayList();
		for (Issue is : issues) {
			data.add(is.getId().toString());
		}
		dependencies.setItems(data);
		dependencies.setCellFactory(TextFieldListCell.forListView());
		dependencies.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		dependencies.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (event.getButton().equals(MouseButton.SECONDARY)) {
					dependencies.getSelectionModel().clearSelection();
				}
			}
		});

		VBox depenBox = new VBox(dependencies);

		ObservableList<Severity> severity = FXCollections.observableArrayList(Severity.values());
		comboBoxSeverity = new ComboBox<Severity>(severity);

		ObservableList<Type> type = FXCollections.observableArrayList(Type.values());
		comboBoxType = new ComboBox<Type>(type);

		descrField.setPrefWidth(200);
		descrField.setWrapText(true);
		descrField.setPrefHeight(100);
		descrField.setPromptText("Enter a description of the issue");

		idField.setPrefColumnCount(2);
		gridPane.add(idLabel, 0, 0);
		gridPane.add(idField, 1, 0);
		idField.setPromptText("Enter an issue ID");
		gridPane.add(nameLabel, 0, 1);
		gridPane.add(nameField, 1, 1);
		gridPane.add(descrLabel, 0, 2);
		gridPane.add(descrField, 1, 2);
		gridPane.add(severLabel, 0, 3);
		gridPane.add(comboBoxSeverity, 1, 3);
		gridPane.add(typeLabel, 0, 4);
		gridPane.add(comboBoxType, 1, 4);
		gridPane.add(dependLabel, 0, 5);
		gridPane.add(depenBox, 1, 5);
		nameField.setPromptText("Enter the issue Name");
		idField.setPrefWidth(200);
		nameField.setPrefWidth(200);

		dependencies.setPrefHeight(dependencies.getItems().size() * 25);
		dependencies.setMaxHeight(100);

		anchorPane.getChildren().add(gridPane);
		GridPane.setMargin(idField, new Insets(20, 0, 20, 20));

		return anchorPane;
	}

	/**
	 * Internal method that adds the buttons for allowing the user to<br>
	 * <ul>
	 * <li>close the window</li>
	 * <li>add the issue with the info provided in the InputPane</li>
	 * </ul>
	 * 
	 * @return the {@link AnchorPane} that contains the buttons
	 */
	private Node createButtonPane() {

		Button cancelButton = new Button("Abbrechen");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.closeView();
			}
		});

		Button addIssueButton = new Button("Issue hinzufügen");
		addIssueButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				String id = idField.getText();
				String name = nameField.getText();
				String description = descrField.getText();
				Set<String> set = new HashSet<>();
				List<String> list = dependencies.getSelectionModel().getSelectedItems();
				for (String item : list) {
					set.add(item);
				}

				Severity severity = (Severity) comboBoxSeverity.getSelectionModel().getSelectedItem();
				Type type = (Type) comboBoxType.getSelectionModel().getSelectedItem();

				controller.addIssue(id, name, description, severity, type, set);

			}
		});

		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.getChildren().addAll(cancelButton, addIssueButton);
		anchorPane.getChildren().addAll(box);
		box.setPadding(new Insets(0, 10, 10, 0));
		box.setSpacing(10);
		AnchorPane.setRightAnchor(box, 10.0);

		return anchorPane;
	}

	/**
	 * Provides an {@link Alert} message if an error occurs.
	 */
	public void showErrorMessage(String string) {
		Alert alert = new Alert(AlertType.ERROR, string);
		alert.showAndWait();
	}
}
