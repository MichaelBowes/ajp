package de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.view;

import java.io.File;
import java.util.Iterator;
import java.util.Optional;

import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.AddIssueController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.IssueDescriptionController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.controller.MainController;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.gui.model.MainModelImpl;
import de.uniba.wiai.dsg.ajp.assignment4.issuetracking.logic.Issue;
import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

public class MainView extends Stage {
	
	private final MainModelImpl model;
	private final MainController controller;
	
	public MainView(MainModelImpl model, MainController controller) {
		this.model = model;
		this.controller = controller;
		Pane rootPane = createRootPane();
		//Binding the title property
		setTitleBinding();
		//setting the scene
		Scene scene = new Scene(rootPane, 550, 300);
		this.setScene(scene);
		this.setOnCloseRequest(new EventHandler<WindowEvent> () {
			public void handle(WindowEvent event) {
				Platform.exit();
				if(!controller.endApplicationDialog()) {
					event.consume();
				}
			}
		});
		this.setMinHeight(300);
		this.setMinWidth(550);
	}
	
	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		
		//Setting elements of the root pane
		root.setTop(createMenuBar());
		root.setCenter(createIssueTable());
		root.setBottom(createButtonPane());
		
		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		return root;
	}
	
	private void setTitleBinding() {
		StringBinding binding = new StringBinding() {
			{
				super.bind(model.getProjectPath(), model.getIssueListProperty());
			}
			@Override
			protected String computeValue() {
				return model.getProjectPath().get() +" - "+ model.getIssueListProperty().getSize() + " issues";
			}		
		};
		this.titleProperty().bind(binding);
	}
	
	/**
	 * Opens an error alert for the user.
	 * @param message - displayed to the user.
	 */
	public void showErrorMessage(String message) {
		Alert alert = new Alert(AlertType.ERROR, message);	
		alert.showAndWait();
	}
	
	public void showInfoMessage(String message) {
		Alert alert = new Alert(AlertType.INFORMATION, message);	
		alert.showAndWait();
	}
	
	/**
	 * Opens a alert window to ask the user for confirmation and return the result.
	 * @param message - displayed to the user.
	 * @return True if the user presses the OK button.<br>
	 * else False
	 */
	public boolean showConfirmationAlert(String message) {
		Alert alert = new Alert(AlertType.CONFIRMATION, message);	
		Optional<ButtonType> result = alert.showAndWait();
		 if (result.isPresent() && result.get() == ButtonType.OK) {
		     return true;
		 }
		 return false;
	}
	
	/**
	 * Opens a file chooser and returns the path of the selected file.
	 * @return Absolute path of the selected file.
	 */
	public String showFileDialog() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));
		File choice = fileChooser.showOpenDialog(this);
		if(choice == null) {
			return null;
		}else {
			return choice.getAbsolutePath();
		}
	}
	
	/**
	 * Displays the view for adding a new Issue.
	 */
	public void showAddIssueView() {
		AddIssueController issueController = new AddIssueController(model);
		issueController.createView();
	}
	
	/**
	 * Displays the view for the description of an issue.
	 * @param issue - for which the description is to be shown.
	 */
	public void showIssueDiscription(Issue issue) {
		IssueDescriptionController descriptionController = new IssueDescriptionController(issue);
		descriptionController.createView();
	}
	
	private Node createMenuBar() {
		MenuBar menuBar = new MenuBar();
		Menu data = new Menu("Datei");
		
		//Creating menu items
		MenuItem newItem = new MenuItem("Neu");
		newItem.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.createNewProject();
			}
		});
		data.getItems().add(newItem);
		
		//Load menu item
		MenuItem loadItem = new MenuItem("Laden");
		loadItem.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.loadProjectDialog();
			}
		});
		data.getItems().add(loadItem);
		
		//Save menu item
		MenuItem saveItem = new MenuItem("Speichern");
		saveItem.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.saveProject();
			}
		});
		data.getItems().add(saveItem);
		
		//Save menu item
		MenuItem saveAsItem = new MenuItem("Speichern Unter");
		saveAsItem.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.saveProjectDialog();
			}
		});
		data.getItems().add(saveAsItem);
		
		//End menu item
		MenuItem endItem = new MenuItem("Beenden");
		endItem.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.endApplicationDialog();
			}
		});
		data.getItems().add(endItem);
		
		menuBar.getMenus().add(data);
		menuBar.setMaxWidth(Double.MAX_VALUE);
		return menuBar;
	}
	
	private Node createButtonPane() {
		
		Button deleteIssueButton = new Button("Ausgew�hlte Issue l�schen");
		deleteIssueButton.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.deleteSelectedIssue();
			}
		});
		
		Button closeIssueButton = new Button("Ausgew�hlte Issue schlie�en");
		closeIssueButton.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.closeSelectedIssue();
			}
		});
		
		Button newIssueButton = new Button("Neuen Issue hinzuf�gen");
		newIssueButton.setOnAction(new EventHandler<ActionEvent> () {
			public void handle(ActionEvent event) {
				controller.addIssueDialog();
			}
		});
		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.setSpacing(10);
		box.getChildren().addAll(deleteIssueButton, closeIssueButton);
		anchorPane.getChildren().addAll(box, newIssueButton);
		AnchorPane.setLeftAnchor(box, 10.0);
		AnchorPane.setRightAnchor(newIssueButton, 10.0);
				
		return anchorPane;
	}
	
	private Node createIssueTable() {
		TableView<Issue> table = new TableView<>();
		
		//Creating the columns for the table
		TableColumn<Issue, String> idColumn = new TableColumn<>("ID");
		TableColumn<Issue, String> nameColumn = new TableColumn<>("Name");
		TableColumn<Issue, String> typeColumn = new TableColumn<>("Typ");
		TableColumn<Issue, String> dependencyColumn = new TableColumn<>("Abh�ngig von");
		TableColumn<Issue, String> severityColumn = new TableColumn<>("Schweregrad");
		TableColumn<Issue, String> statusColumn = new TableColumn<>("Status");
			
		
		//Adding the columns to the table
		table.getColumns().add(idColumn);
		table.getColumns().add(nameColumn);
		table.getColumns().add(typeColumn);
		table.getColumns().add(dependencyColumn);
		table.getColumns().add(severityColumn);
		table.getColumns().add(statusColumn);
		
		//Setting the column size
		idColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		nameColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		typeColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		dependencyColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		severityColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		statusColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		
		//Creating the property values for the columns
		PropertyValueFactory<Issue, String> idColFactory = new PropertyValueFactory<>("id");
		PropertyValueFactory<Issue, String> nameColFactory = new PropertyValueFactory<>("name");
		PropertyValueFactory<Issue, String> typeColFactory = new PropertyValueFactory<>("type");	
		
		Callback<CellDataFeatures<Issue, String>, ObservableValue<String>> dependencyColFactory = new Callback<CellDataFeatures<Issue, String>,
				ObservableValue<String>>(){

			@Override
			public ObservableValue<String> call(CellDataFeatures<Issue, String> arg0) {				
				String result = "";
				Iterator<Issue> iterator = arg0.getValue().getDependencies().iterator();
				while(iterator.hasNext()) {
					result += iterator.next().getId();
					if(iterator.hasNext()) {
						result += ", ";
					}
				}		
				SimpleStringProperty prop = new SimpleStringProperty(result);
				return prop;
			}			
		};		
		
		PropertyValueFactory<Issue, String> severityColFactory = new PropertyValueFactory<>("severity");
		PropertyValueFactory<Issue, String> statusColFactory = new PropertyValueFactory<>("state");
		
		//Setting the column value factories
		idColumn.setCellValueFactory(idColFactory);
		nameColumn.setCellValueFactory(nameColFactory);
		typeColumn.setCellValueFactory(typeColFactory);
		dependencyColumn.setCellValueFactory(dependencyColFactory);
		severityColumn.setCellValueFactory(severityColFactory);
		statusColumn.setCellValueFactory(statusColFactory);
		
		//Add event for double click on table rows
		table.setRowFactory(tv -> {
	         TableRow<Issue> row = new TableRow<>();
	         row.setOnMouseClicked(event -> {
	             if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
	                 Issue rowData = row.getItem();
	                 controller.displayIssueDetails(rowData);
	             }
	         });
	         return row;
	    });
		//Add listener to change the selection in the model
		table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Issue>() {

			@Override
			public void changed(ObservableValue<? extends Issue> arg0, Issue oldValue, Issue newValue) {
				model.setSelectedIssue(newValue);				
			}
			
		});
		
		table.itemsProperty().bind(model.getIssueListProperty());
		
		//Create ScrollPane container
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(table);
		scrollPane.setFitToHeight(true);
		scrollPane.setFitToWidth(true);
		table.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		scrollPane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		return scrollPane;
	}
}
